<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GetController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/biodata', [GetController::class, 'biodata']);
Route::get('/contact', [GetController::class, 'contact']);
Route::get('/index',[GetController::class, 'index']);
Route::get('/pengalaman', [GetController::class, 'pengalaman']);

