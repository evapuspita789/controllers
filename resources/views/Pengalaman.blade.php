@extends('navbar')
@section('navbar')
        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Eva Puspita</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- Pricing Start -->
        <div class="price">
            <div class="container">
                <div class="section-header text-center">
                    <p>Eva Puspita</p>
                    <h2>Pengalaman</h2>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="price-item">
                            <div class="price-text">
                                <p>Juara III Lomba Senam </p>
                                <h2>Tahun 2019</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="price-item">
                            <div class="price-text">
                                <p>Juara I Permainan Edukasi</p>
                                <h2>Tahun 2021</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="price-item">
                            <div class="price-text">
                                <p>Juara II Permainan Edukasi </p>
                                <h2>Tahun 2020 </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="price-item">
                            <div class="price-text">
                                <p>Juara II Video Pembelajaran</p>
                                <h2>Tahun 2021</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pricing End -->
        @endsection