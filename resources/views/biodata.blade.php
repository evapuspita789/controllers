@extends('navbar')
@section('navbar')
        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Biodata</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- Blog Start -->
        <div class="blog"> 
            <div class="container">
                <div class="section-header text-center">
                    <h2>Komang Eva Puspita Dewi</h2>
                </div>
                <center>
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="img/eva.png" alt="Blog">
                            </div>
                            <div class="blog-meta">
                                <i class="fa fa-list-alt"></i>
                                <a href="">Mahasiswa</a>
                                <i class="fa fa-calendar-alt"></i>
                                <p>21-Juli-2001</p>
                            </div>
                            <div class="blog-text">
                                <h2>Program Studi Sistem Informasi</h2>
                                <p>
                                    Universitas Pendidikan Ganesha 
                                    Semester V
                                </p>
                        </div>
                    </div>
                </center>
                @endsection