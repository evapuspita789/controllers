@extends('navbar')
@section('navbar')
        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Contact</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- Contact Start -->
        <div class="section-header text-center" style="margin-top: 90px;">
            <p>Jika ada yang ingin ditanyakan</p>
            <h2>Hubungi 0819 0537 7233 Via WhatsApp</h2>
                <div class="social">
                    <a href="https://twitter.com/merryriana?lang=en"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.facebook.com/MerryRiana"><i class="fab fa-facebook-f"></i></a>
                    <a href="https://id.linkedin.com/in/merryriana"><i class="fab fa-linkedin-in"></i></a>
                    <a href="https://www.instagram.com/merryriana/?hl=en"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
        <!-- Contact End -->
        @endsection

