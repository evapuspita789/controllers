<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Curriculum Vitae</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free Website Template" name="keywords">
        <meta content="Free Website Template" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">

        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>

    <body>
        <!-- Top Bar Start -->
        <div class="top-bar d-none d-md-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="top-bar-left">
                            <div class="text">
                                <h2>8:00 - 19:00</h2>
                                <p>Opening Hour Mon - Fri</p>
                            </div>
                            <div class="text">
                                <h2>081 717 310 443</h2>
                                <p>Call Us For Appointment</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        
                        <div class="top-bar-right">
                            <div class="social">
                                <a href=""><i class="fab fa-youtube"></i></a>
                                <a href="https://www.facebook.com/evha.pusphitathewi"><i class="fab fa-facebook-f"></i></a>
                                <a href="https://www.linkedin.com/in/komang-eva-puspita-dewi-0a95661a2"><i class="fab fa-linkedin-in"></i></a>
                                <a href="https://instagram.com/evha_oii1?utm_medium=copy_link"><i class="fab fa-instagram"></i></a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Bar End -->

        <!-- Nav Bar Start -->
        
        <div class="navbar navbar-expand-lg bg-dark navbar-dark">
            
            <div class="container-fluid">
                <a href="index" class="navbar-brand">Curriculum <span>Vitae</span></a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav ml-auto">
                        <a href="index" class="nav-item nav-link ">Beranda</a>
                        <a href="biodata" class="nav-item nav-link active">Biodata</a>
                        <a href="pengalaman" class="nav-item nav-link">Pengalaman</a>
                        <a href="contact" class="nav-item nav-link">Kontak</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        @yield('navbar')
        <!-- Nav Bar End -->
    </body>
    </html>